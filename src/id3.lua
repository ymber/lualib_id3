id3 = {}

local function convert_from_syncsafe( syncsafe_int )
    -- Tag size is stored as a 32 bit syncsafe integer
    return syncsafe_int:sub( 1, 1 ):byte() * 2048383 +
           syncsafe_int:sub( 2, 2 ):byte() * 16129 +
           syncsafe_int:sub( 3, 3 ):byte() * 127 +
           syncsafe_int:sub( 4, 4 ):byte()
end

function id3.utf16_to_utf8( utf16_string )
    -- This function will only work with input characters that can be represented with one byte
    utf8_string = ""
    for i = 1, utf16_string:len() do
        if i % 2 ~= 0 then
            -- Remove byte order marks from string
            -- Sum of byte order mark bytes for BE or LE is 509
            if utf16_string:sub( i, i ):byte() + utf16_string:sub( i + 1, i + 1 ):byte() ~= 509 then
                utf8_string = utf8_string .. string.char( utf16_string:sub( i, i ):byte() + utf16_string:sub( i + 1, i + 1 ):byte() )
            end
        end
    end

    return utf8_string
end

function id3.read_header( file )
    file:seek( set, 0 )
    local header = file:read( 10 )

    local head = header:sub( 1, 3 )
    local version_major = header:sub( 4, 4 ):byte()
    local version_minor = header:sub( 5, 5 ):byte()
    local flags = header:sub( 6, 6 )
    local size = convert_from_syncsafe( header:sub( 7, 10 ) )

    return { head, version_major, version_minor, flags, size }
end

function id3.read_frame( file, seek_position )
    file:seek( "set", seek_position )
    local frame_header = file:read( 10 )

    local frame_id = frame_header:sub( 1, 4 )
    local frame_size = convert_from_syncsafe( frame_header:sub( 5, 8 ) )
    local frame_flags1 = frame_header:sub( 9, 9 )
    local frame_flags2 = frame_header:sub( 10, 10 )
    local data = nil
    if frame_size > 0 then data = file:read( frame_size ) end

    return { frame_id, frame_size, frame_flags1, frame_flags2, data}
end

function id3.frames_dict( file, tag_size )
    -- Frames begin after 10 byte tag header
    local seek_position = 10
    file:seek( "set", seek_position )

    local frames = {}
    
    while true do
        if seek_position > tag_size then break end
        frame = id3.read_frame( file, seek_position )
        seek_position = seek_position + frame[2] + 10
        table.insert( frames, frame )
    end

    return frames
end

function id3.text_frame_data( frame_data )
    local encoding = frame_data:sub( 1, 1 ):byte()
    local text = nil

    -- Byte order mark is used only with UTF-16
    if encoding == 1 then
        text = frame_data:sub( 4 )
    else
        text = frame_data:sub( 2 )
    end

    return { encoding, text }
end

function id3.txxx_frame_data( frame_data )
    local encoding = frame_data:sub( 1, 1 )
    local text = nil

    -- Byte order mark is used only with UTF-16
    if encoding == 1 then
        text = frame_data:sub( 4 )
    else
        text = frame_data:sub( 2 )
    end

    -- text is a string containing a null terminated frame descriptor followed by the frame content
    -- TXXX frames can contain multiple null terminated strings
    local strings = {}
    for str in string.gmatch( id3.utf16_to_utf8( text ), "%Z" ) do
        table.insert( strings, str )
    end

    return { encoding, strings }
end

return id3
