# Simple lua ID3 parsing library

This library contains functions to extract and process a subset of the frames defined in the ID3v2.3.0 and ID3v2.4.0 specifications.

## Limitations

This library does not support:

* extended headers
* footers
* byte order marks for UTF-16 encoding
* flags
